package testen;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRekenmachine {
	@Test
	public void sommeer() {
		Rekenmachine rekenmachine = new Rekenmachine();
		double resultaat = rekenmachine.sommeer(10, 50);
		assertEquals(60.1, resultaat, "De som van 10 en 50 moet 60 zijn");
	}
}
