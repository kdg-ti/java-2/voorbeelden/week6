package testen;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class PersoonTest {
    private static Persoon pers1;
    private static Persoon pers2;

    @BeforeAll
    public static void init() {
        pers1 = new Persoon("liesa");
        pers2 = new Persoon("elisa");
    }

    @Test
    public void testEquals() {
        assertTrue( pers1.equals(pers1),"De personen moeten gelijk zijn");
        assertTrue( pers1.equals(new Persoon("liesa")),"De personen moeten gelijk zijn");
        assertFalse( pers1.equals(pers2),"De personen moeten verschillen");
        assertFalse( pers1.equals(null),"Vergelijken met null moet false geven");
        assertFalse( pers1.equals(Integer.valueOf(1)),"Vergelijken met een ander type moet false geven");
    }

    @Test
    public void testHashcode() {
        int hashcode1 = pers1.hashCode();
        Persoon persoon = new Persoon("liesa");
        int hashcode2 = persoon.hashCode();
        int hashcode3 = pers2.hashCode();

        assertEquals(hashcode1,hashcode2,"De hashcodes moeten gelijk zijn");
        assertNotEquals( hashcode1 , hashcode3,"De hashcodes moeten verschillend zijn");
    }
}

